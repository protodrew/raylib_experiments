#include "raylib.h"
#include <stdlib.h>
#include <time.h>
int main(void)
{
    // Initialization
    //--------------------------------------------------------------------------------------
    const int SCREEN_WIDTH = 500;
    const int SCREEN_HEIGHT = 500;
    int opacity = 255;
    Color bkg = CLITERAL(Color){232, 237, 223,255};
    Color colors[5]={CLITERAL(Color){ 144, 252, 249, opacity },CLITERAL(Color){ 255, 188, 66, opacity },CLITERAL(Color){ 179, 151, 230, opacity },CLITERAL(Color){ 237, 106, 94, opacity },CLITERAL(Color){ 55, 119, 113, opacity } };
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "POP_GRID");
    srand(time(0));
    SetTargetFPS(1);
    //--------------------------------------------------------------------------------------

    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
            BeginDrawing();
            ClearBackground(bkg);
            opacity = 255;
            for(int x = 0; x < SCREEN_WIDTH; x += 75){
                for(int y = 0; y < SCREEN_HEIGHT; y += 75){
                    int index = (rand() %4);
                    colors[index].a = opacity;
                    DrawRectangle(x, y, 65, 65, colors[index]);
                }
            }
            opacity = 100;
            for(int x = 0; x < SCREEN_WIDTH; x += 75){
                for(int y = 0; y < SCREEN_HEIGHT; y += 75){
                    int index = (rand() %4);
                    colors[index].a = opacity;
                    DrawRectangle(x + 25, y + 25, 75, 75, colors[index]);
                }
            }
            
            
        EndDrawing();
        
    }

    // De-Initialization
    CloseWindow();        // Close window and OpenGL context
    return 0;
}