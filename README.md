# Raylib Experiments

This is the place where I will post all of the generative art pieces I work on in order to learn [raylib](https://raylib.com). I work on most of these on my [stream](https://twitch.tv/protodrew)

### Running

Simply clone the repo, go into the folder you want to run, and run 

```
make
./game
```



License
AGPL 3.0 (see [LICENSE.md](LICENSE.md))
