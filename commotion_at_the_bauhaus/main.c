#include "raylib.h"
#include <stdio.h>
#include <math.h>
int main(void)
{
    /* Color Definitions */
    #define baublack   CLITERAL(Color){ 37, 39, 40, 255 } 
    #define baugrey   CLITERAL(Color){ 51, 51, 51, 255 } 
    #define bauyellow   CLITERAL(Color){ 243, 217, 69, 255 } 
    #define baured   CLITERAL(Color){ 192, 59, 30, 255 } 
    #define baublue   CLITERAL(Color){ 4, 85, 116, 255 } 
    #define bauteal   CLITERAL(Color){ 118, 156, 168, 255 } 
    #define bauwhite   CLITERAL(Color){ 227, 236, 229, 255 } 
    /*-------------------*/
    
    const int SCREEN_WIDTH = 600;
    const int SCREEN_HEIGHT = 900;
    const int CIRC_MIN = 350;
    const int CIRC_MAX = 525;
    const int CIRC_RAD_MIN = 40;
    const int CIRC_RAD_MAX = 130;
    const int RECT_MAX = 470;
    const int RECT_MIN = 300;
    InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "commotion at the bauhaus");
    SetWindowPosition(2500,100);
    SetTargetFPS(48);
    int frame = 1;
    int circx = CIRC_MIN;
    int circrad = CIRC_RAD_MIN;
    int rectx = RECT_MAX;
    int circForward = 1;
    int circGrow = 1;
    int rectForward = 0;
    int ang = 0;
    int ang2 = 0;
    Color circ1;
    Color circ2;
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        
        float secPerc = trunc(GetTime()*100)/100-(int)GetTime(); // a value between 0 and .99 that represents the percentage of the way through a second we are

        BeginDrawing();

            ClearBackground(baublack);
            



            if(circx >= CIRC_MAX){
                circForward = 0;
            }
            if(circx <= CIRC_MIN){
                circForward = 1;
            }

            if(circForward == 1){
                circx+=2;
            }else{
                circx-=2;
            }

            if(rectx >= RECT_MAX){
                rectForward = 0;
            }
            if(rectx <= RECT_MIN){
                rectForward = 1;
            }

            if(rectForward == 1){
                rectx+=2;
            }else{
                rectx-=2;
            }
            

            DrawRectangle(0, 200, 60, 80, baured);
            DrawRectangle(0, 300, 130, 280, baured);
            DrawRectangle(130, 0, 400, 300, bauyellow);
            
            DrawRectangle(130, 300, 400, 280, BLACK);
            DrawRectangle(530, 0, 70, 180, baublue);
            DrawRectangle(530, 180, 70, 120, baured);
            
            
            DrawRectangle(0, 581, 600, 319, baugrey);
            DrawRectangle(rectx, 615, 100, 100, bauwhite);
            DrawCircle(circx, 665, 50, BLACK);

            if(secPerc < 0.33){
                DrawRectangle(530, 330, 70, 70, bauyellow);
                DrawRectangle(530, 420, 70, 70, baublue);
                DrawRectangle(530, 510, 70, 70, baured);

                for(int y = 0; y < 180; y += 30){
                    DrawRectangle(530, y, 70, 10, BLACK);
                }
            }
            else if(secPerc > 0.66){
                DrawRectangle(530, 330, 70, 70, baublue);
                DrawRectangle(530, 420, 70, 70, baured);
                DrawRectangle(530, 510, 70, 70, bauyellow);
                for(int y = 10; y < 180; y += 30){
                    DrawRectangle(530, y, 70, 10, BLACK);
                }
            }
            else{
                DrawRectangle(530, 330, 70, 70, baured);
                DrawRectangle(530, 420, 70, 70, bauyellow);
                DrawRectangle(530, 510, 70, 70, baublue);
                for(int y = 20; y < 180; y += 30){
                    DrawRectangle(530, y, 70, 10, BLACK);
                }
            }

            if(secPerc < 0.25 || (secPerc > 0.5 && secPerc < 0.75)){
                for(int y = 320; y < 560; y+= 40){
                    DrawRectangle(0, y, 115, 20, bauwhite);  
                }
                for(int x = 130; x < 510; x+= 40){
                    DrawRectangle(x, 450, 20, 130, bauteal);
                }
            }
            else{
                for(int y = 340; y < 560; y+= 40){
                    DrawRectangle(0, y, 115, 20, bauwhite);  
                }
                
                for(int x = 150; x < 550; x+= 40){
                    DrawRectangle(x, 450, 20, 130, bauteal);
                }
            }
            ang += 4;
            ang2 -= 3;
            DrawCircleSector((Vector2){0,50}, 80, ang-90, ang+90, 90, bauwhite);
            DrawCircleSector((Vector2){450,905}, 120, ang2-90, ang2+90, 90, baured);
            DrawCircleSector((Vector2){450,905}, 30, ang2-90, ang2+90, 90, bauwhite);
            DrawTriangle((Vector2){0,650},
                         (Vector2){0,920},
                         (Vector2){300,785}, baublue);
            
            DrawCircle(330, 290, 120, baublue);

            if(circrad >= CIRC_RAD_MAX){
                circGrow = 0;
            }
            if(circrad <= CIRC_RAD_MIN){
                circGrow = 1;
            }

            if(circGrow == 1){
                circrad+=2;
            }else{
                circrad-=2;
            }
            
            DrawCircle(330, 290, circrad, bauwhite);
            DrawCircle(250, 100, 50, baublack);
            DrawCircle(410, 100, 50, baured);

            
        EndDrawing();
    
    }

    CloseWindow();


    return 0;
}
